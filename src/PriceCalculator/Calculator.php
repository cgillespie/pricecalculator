<?php

namespace PriceCalculator;
use PriceCalculator\ExpressionLanguage;

define('PHP_INT_MIN', 255<<(PHP_INT_SIZE*8)-1);

class Calculator
{
    const TYPE_PERCENTAGE = 'PERCENTAGE';
    const TYPE_MULTIPLY = 'MULTIPLY';
    const TYPE_ADD = 'ADD';
    const TYPE_SUBTRACT = 'SUBTRACT';
    const TYPE_DIVIDE = 'DIVIDE';
    const TYPE_FORCE = 'FORCE';
    const TYPE_FORMULA = 'FORMULA';

    protected $min = 0;
    protected $max = PHP_INT_MAX;
    protected $maxDrop = PHP_INT_MAX;
    protected $maxRise = PHP_INT_MAX;
    protected $decimalPlaces = 2;
    protected $adjustmentType = Calculator::TYPE_ADD;
    protected $adjustmentValue = 0;
    protected $enforcePriceWithinMinMax = true;

    private function enforceRange($value, $min, $max)
    {
        if ( !is_numeric($value) || $value < $min || $value > $max ) {
            throw new \InvalidArgumentException("$value is not between $min and $max");
        }
    }

    private function enforceChoice($value, $options = array())
    {
        if ( !in_array($value, $options) ) {
            throw new \InvalidArgumentException("$value is not a valid value");
        }
    }

    function validate()
    {
        $this->enforceRange($this->getMin(), 0, is_null($this->getMax()) ? PHP_INT_MAX : $this->getMax());
        $this->enforceRange($this->getMax(), is_null($this->getMin()) ? 0 : $this->getMin(), PHP_INT_MAX);        
        $this->enforceRange($this->getMaxDrop(), 0, PHP_INT_MAX);
        $this->enforceRange($this->getMaxRise(), 0, PHP_INT_MAX);
        $this->enforceRange($this->getDecimalPlaces(), 0, 10);

        if ( $this->getAdjustmentType() !== Calculator::TYPE_FORMULA ) {
            $this->enforceRange($this->getAdjustmentValue(), PHP_INT_MIN, PHP_INT_MAX);
        }

        $this->enforceChoice($this->getAdjustmentType(), Calculator::validAdjustmentTypes());
        $this->enforceChoice($this->getEnforcePriceWithinMinMax(), array(true, false));
    }       

    static function validAdjustmentTypes() {
        return array(
            Calculator::TYPE_PERCENTAGE, 
            Calculator::TYPE_MULTIPLY, 
            Calculator::TYPE_ADD, 
            Calculator::TYPE_SUBTRACT, 
            Calculator::TYPE_DIVIDE, 
            Calculator::TYPE_FORCE,
            Calculator::TYPE_FORMULA
        );
    }

    function adjust($price)
    {
        $this->validate();

        if ( !is_numeric($price) ) {
            throw new \InvalidArgumentException("'$price' is not a valid price");
        }

        if ( $this->getEnforcePriceWithinMinMax() ) {
            if ( $price < $this->getMin() ) {
                throw new \InvalidArgumentException("'$price' must not be less than minimum '{$this->getMin()}'");
            }

            if ( $price > $this->getMax() ) {
                throw new \InvalidArgumentException("'$price' must not be greater than maximum '{$this->getMax()}'");
            }
        }

        $newPrice = $this->doUnconstrainedAdjustment($price);
        $newConstrainedPrice = max($this->getEffectiveMin($price), min($this->getEffectiveMax($price), $newPrice));
        return round($newConstrainedPrice, $this->getdecimalPlaces());
    }	

    private function doUnconstrainedAdjustment($price)
    {
        $type = $this->getAdjustmentType();

        switch($type)
        {
        case Calculator::TYPE_PERCENTAGE:
            return $price * (1.0 + ($this->getAdjustmentValue()/100));
        case Calculator::TYPE_MULTIPLY:
            return $price * $this->getAdjustmentValue();
        case Calculator::TYPE_ADD:    
            return $price + $this->getAdjustmentValue();            
        case Calculator::TYPE_SUBTRACT:
            return $price - $this->getAdjustmentValue();             
        case Calculator::TYPE_DIVIDE:
            return $price / $this->getAdjustmentValue();             
        case Calculator::TYPE_FORCE:
            return $this->getAdjustmentValue();
        case Calculator::TYPE_FORMULA:
            $language = new ExpressionLanguage();
            return $language->evaluate($this->getAdjustmentValue(), array('price' => $price));
        }        
    }

    public function getEffectiveMin($price)
    {
        $populatedMin = is_null($this->getMin()) ? 0 : $this->getMin();
        $populatedDrop = is_null($this->maxDrop) ? 0 : $price - $this->maxDrop;
        return max($populatedMin, $populatedDrop);
    }

    public function getEffectiveMax($price)
    {
        $populatedMax = is_null($this->getMax()) ? PHP_INT_MAX : $this->getMax();
        $populatedRise = is_null($this->maxRise) ? PHP_INT_MAX : $price + $this->maxRise;
        return min($populatedMax, $populatedRise);
    }

    /**
     * Gets the this->getAdjustmentValue() of min.
     *
     * @return mixed
     */
    public function getMin()
    {
        return $this->min;
    }

    /**
     * Sets the this->getAdjustmentValue() of min.
     *
     * @param mixed $min the min
     *
     * @return self
     */
    public function setMin($min)
    {
        $this->min = $min;

        return $this;
    }

    /**
     * Gets the this->getAdjustmentValue() of max.
     *
     * @return mixed
     */
    public function getMax()
    {
        return $this->max;
    }

    /**
     * Sets the this->getAdjustmentValue() of max.
     *
     * @param mixed $max the max
     *
     * @return self
     */
    public function setMax($max)
    {
        $this->max = $max;

        return $this;
    }

    /**
     * Gets the this->getAdjustmentValue() of maxDrop.
     *
     * @return mixed
     */
    public function getMaxDrop()
    {
        return $this->maxDrop;
    }

    /**
     * Sets the this->getAdjustmentValue() of maxDrop.
     *
     * @param mixed $maxDrop the max drop
     *
     * @return self
     */
    public function setMaxDrop($maxDrop)
    {
        $this->maxDrop = $maxDrop;

        return $this;
    }

    /**
     * Gets the this->getAdjustmentValue() of maxRise.
     *
     * @return mixed
     */
    public function getMaxRise()
    {
        return $this->maxRise;
    }

    /**
     * Sets the this->getAdjustmentValue() of maxRise.
     *
     * @param mixed $maxRise the max rise
     *
     * @return self
     */
    public function setMaxRise($maxRise)
    {
        $this->maxRise = $maxRise;

        return $this;
    }

    /**
     * Gets the value of decimalPlaces.
     *
     * @return mixed
     */
    public function getDecimalPlaces()
    {
        return $this->decimalPlaces;
    }

    /**
     * Sets the value of decimalPlaces.
     *
     * @param mixed $decimalPlaces the decimal places
     *
     * @return self
     */
    public function setDecimalPlaces($decimalPlaces)
    {
        $this->decimalPlaces = $decimalPlaces;

        return $this;
    }

    /**
     * Gets the value of adjustmentType.
     *
     * @return mixed
     */
    public function getAdjustmentType()
    {
        return $this->adjustmentType;
    }

    /**
     * Sets the value of adjustmentType.
     *
     * @param mixed $adjustmentType the adjustment type
     *
     * @return self
     */
    public function setAdjustmentType($adjustmentType)
    {
        $this->adjustmentType = $adjustmentType;

        return $this;
    }

    /**
     * Gets the value of adjustmentValue.
     *
     * @return mixed
     */
    public function getAdjustmentValue()
    {
        return $this->adjustmentValue;
    }

    /**
     * Sets the value of adjustmentValue.
     *
     * @param mixed $adjustmentValue the adjustment value
     *
     * @return self
     */
    public function setAdjustmentValue($adjustmentValue)
    {
        $this->adjustmentValue = $adjustmentValue;

        return $this;
    }

    /**
     * Gets the value of enforcePriceWithinMinMax.
     *
     * @return mixed
     */
    public function getEnforcePriceWithinMinMax()
    {
        return $this->enforcePriceWithinMinMax;
    }

    /**
     * Sets the value of enforcePriceWithinMinMax.
     *
     * @param mixed $enforcePriceWithinMinMax the enforce price within min max
     *
     * @return self
     */
    public function setEnforcePriceWithinMinMax($enforcePriceWithinMinMax)
    {
        $this->enforcePriceWithinMinMax = $enforcePriceWithinMinMax;

        return $this;
    }
}
