<?php

namespace PriceCalculator;

use Symfony\Component\ExpressionLanguage\ExpressionLanguage as BaseExpressionLanguage;
use Symfony\Component\ExpressionLanguage\SyntaxError;

class ExpressionLanguage extends BaseExpressionLanguage
{
    protected function registerFunctions()
    {
        parent::registerFunctions();
        $this->registerMin();
        $this->registerMax();
        $this->registerCeil();
        $this->registerFloor();
        $this->registerRand();
    }

    protected function registerMin()
    {
        $this->register(
            'min',
            function ($a, $b) {
                if ( !is_numeric($a) || !is_numeric($b) ) {
                    throw new SyntaxError("You must pass two values into 'min'");
                }

                return sprintf('min(%s, %s)', $a, $b);
            }, function ($args, $a, $b) {              
                if ( !is_numeric($a) || !is_numeric($b) ) {
                    throw new SyntaxError("You must pass two values into 'min'");
                }

                return min($a, $b);
            }
        );
    }

    protected function registerMax()
    {
        $this->register(
            'max', 
            function ($a, $b) {
                if ( !is_numeric($a) || !is_numeric($b) ) {
                    throw new SyntaxError("You must pass two values into 'max'");
                }

                return sprintf('max(%s, %s)', $a, $b);
            }, function ($args, $a, $b) {
                if ( !is_numeric($a) || !is_numeric($b) ) {
                    throw new SyntaxError("You must pass two values into 'max'");
                }

                return max($a, $b);
            }
        );
    }

    protected function registerCeil()
    {
        $this->register(
            'ceil', 
            function ($a) {
                if ( !is_numeric($a) ) {
                    throw new SyntaxError("You must pass a value into 'ceil'");
                }

                return sprintf('ceil(%s)', $a);
            }, function ($args, $a) {
                if ( !is_numeric($a) ) {
                    throw new SyntaxError("You must pass a value into 'ceil'");
                }

                return ceil($a);
            }
        );
    }

    protected function registerFloor()
    {
        $this->register(
            'floor', 
            function ($a) {
                if ( !is_numeric($a) ) {
                    throw new SyntaxError("You must pass a value into 'floor'");
                }

                return sprintf('floor(%s)', $a);
            }, function ($args, $a) {
                if ( !is_numeric($a) ) {
                    throw new SyntaxError("You must pass a value into 'floor'");
                }

                return floor($a);
            }
        );
    }

    protected function registerRand()
    {
        $this->register(
            'rand', 
            function ($a, $b) {
                if ( !is_numeric($a) || !is_numeric($b) ) {
                    throw new SyntaxError("You must pass two values into 'rand'");
                }

                return sprintf('rand(%s, %s)', $a, $b);
            }, function ($args, $a, $b) {
                if ( !is_numeric($a) || !is_numeric($b) ) {
                    throw new SyntaxError("You must pass two values into 'rand'");
                }

                return rand($a, $b);
            }
        );
    }
}