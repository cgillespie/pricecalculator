<?php

namespace Calculator\Test;

use PriceCalculator\Calculator;

class CalculatorTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @expectedException InvalidArgumentException
     */
    function testFailsIfNoNumber() 
    {
        $calculator = new Calculator();
        $calculator->setAdjustmentType(Calculator::TYPE_MULTIPLY);
        $calculator->setAdjustmentValue(0.8);
        $calculator->adjust('Foo');
    }

    /**
     * @expectedException InvalidArgumentException
     */
    function testFailsIfNotValidType()
    {
        $calculator = new Calculator();
        $calculator->setAdjustmentType('Foo');
        $calculator->setAdjustmentValue(0.8);
        $calculator->adjust(100);
    }

    /**
     * @expectedException InvalidArgumentException
     */
    function testFailsIfNotValidValue()
    {
        $calculator = new Calculator();
        $calculator->setAdjustmentType(Calculator::TYPE_ADD);
        $calculator->setAdjustmentValue('Foo');
        $calculator->adjust(100);
    }

    /**
     * @expectedException InvalidArgumentException
     */
    function testFailsIfPriceGreaterThanMax()
    {
        $calculator = new Calculator();
        $calculator->setMax(100);
        $calculator->setAdjustmentType(Calculator::TYPE_ADD);
        $calculator->setAdjustmentValue(0.8);        
        $calculator->adjust(110);
    }

    /**
     * @expectedException Symfony\Component\ExpressionLanguage\SyntaxError
     */
    function testFailsIfBadFormula()
    {
        $calculator = new Calculator();
        $calculator->setAdjustmentType(Calculator::TYPE_FORMULA);
        $calculator->setAdjustmentValue('price * (1.0');        
        $calculator->adjust(100);
    }

    /**
     * @expectedException Symfony\Component\ExpressionLanguage\SyntaxError
     */
    function testFailsIfBadFormulaFunctionMin()
    {
        $calculator = new Calculator();
        $calculator->setAdjustmentType(Calculator::TYPE_FORMULA);
        $calculator->setAdjustmentValue('min(1, "what")');        
        $calculator->adjust(100);
    }

    /**
     * @expectedException Symfony\Component\ExpressionLanguage\SyntaxError
     */
    function testFailsIfBadFormulaFunctionMax()
    {
        $calculator = new Calculator();
        $calculator->setAdjustmentType(Calculator::TYPE_FORMULA);
        $calculator->setAdjustmentValue('max(1, "what")');        
        $calculator->adjust(100);
    }

    /**
     * @expectedException Symfony\Component\ExpressionLanguage\SyntaxError
     */
    function testFailsIfBadFormulaFunctionCeil()
    {
        $calculator = new Calculator();
        $calculator->setAdjustmentType(Calculator::TYPE_FORMULA);
        $calculator->setAdjustmentValue('ceil("what")');        
        $calculator->adjust(100);
    }

    /**
     * @expectedException Symfony\Component\ExpressionLanguage\SyntaxError
     */
    function testFailsIfBadFormulaFunctionFloor()
    {
        $calculator = new Calculator();
        $calculator->setAdjustmentType(Calculator::TYPE_FORMULA);
        $calculator->setAdjustmentValue('floor("what")');        
        $calculator->adjust(100);
    }

    /**
     * @expectedException Symfony\Component\ExpressionLanguage\SyntaxError
     */
    function testFailsIfBadFormulaFunctionRand()
    {
        $calculator = new Calculator();
        $calculator->setAdjustmentType(Calculator::TYPE_FORMULA);
        $calculator->setAdjustmentValue('rand(1, "what")');        
        $calculator->adjust(100);
    }

    /**
     * @expectedException InvalidArgumentException
     */
    function testFailsIfPriceLessThanMin()
    {
        $calculator = new Calculator();
        $calculator->setMin(100);
        $calculator->setAdjustmentType(Calculator::TYPE_ADD);
        $calculator->setAdjustmentValue(0.8);        
        $calculator->adjust(90);
    }

    function testDoesNotFailIfPriceLessThanMinAndEnforceDisabled()
    {
        $calculator = new Calculator();
        $calculator->setMin(100);
        $calculator->setEnforcePriceWithinMinMax(false);
        $calculator->setAdjustmentType(Calculator::TYPE_ADD);
        $calculator->setAdjustmentValue(0.8);        
        $calculator->adjust(90);
        $this->assertTrue(true, true);
    }

    function calculationDataProvider() {
        return array(
            array(Calculator::TYPE_PERCENTAGE, 100, 10, 110),
            array(Calculator::TYPE_PERCENTAGE, 100, -10, 90),
            array(Calculator::TYPE_MULTIPLY, 100, 0.8, 80),
            array(Calculator::TYPE_DIVIDE, 100, 0.8, 125),
            array(Calculator::TYPE_ADD, 100, 1, 101),
            array(Calculator::TYPE_SUBTRACT, 100, 1, 99),
            array(Calculator::TYPE_FORCE, 100, 200, 200),
        );
    }

    /**
     * @dataProvider calculationDataProvider
     */
    function testCalculates($type, $value, $adjustment, $result) 
    {
        $calculator = new Calculator();
        $calculator->setAdjustmentType($type);
        $calculator->setAdjustmentValue($adjustment);
        $this->assertEquals($result, $calculator->adjust($value));
    }

    function formulaCalculationDataProvider() {
        return array(
            array(100, 'price * 1.1', 110),
            array(100, 'price - 10', 90),
            array(100, 'price * 0.8', 80),
            array(100, 'price / 0.8', 125),
            array(100, 'price + 1', 101),
            array(100, 'price - 1', 99),
            array(100, '(price * 0.973) - 0.01', 97.29),
            array(100, '(price * 0.973) - 0.01 + price / 33.2', 100.3),
            array(100, 'min(price, 30)', 30),
            array(100, 'min(30, price)', 30),
            array(100, 'max(30, price)', 100),
            array(100, 'max(price, 30)', 100),
            array(100, 'max(price, min(400,500))', 400),
            array(100, 'min(price, min(400,500))', 100),
            array(100, 'min(price, min(10,500))', 10),
            array(100, 'floor(10.99)', 10),
            array(100, 'ceil(2.5)', 3),
            array(100, 'rand(1,1)', 1)
        );
    }

    /**
     * @dataProvider formulaCalculationDataProvider
     */
    function testCalculatesFormula($value, $formula, $result) 
    {
        $calculator = new Calculator();
        $calculator->setAdjustmentType(Calculator::TYPE_FORMULA);
        $calculator->setAdjustmentValue($formula);
        $this->assertEquals($result, $calculator->adjust($value));
    }

    function effectiveMinDataProvider() {
        return array(
            array(0,100,50,0),
            array(0,10,50,40),
            array(40,100,50,40),
            array(40,30,50,40),
            array(40,0,40,40),
            array(null,0,40,40),
            array(null,20,40,20),
            array(40,null,40,40),
            array(null,null,40,0),
        );
    }

    /**
     * @dataProvider effectiveMinDataProvider
     */
    function testCalculatesEffectiveMin($min, $maxDrop, $price, $result) 
    {
        $calculator = new Calculator();
        $calculator->setMin($min);
        $calculator->setMaxDrop($maxDrop);
        $this->assertEquals($result, $calculator->getEffectiveMin($price));
    }  


    function effectiveMaxDataProvider() {
        return array(
            array(120,10,100,110),
            array(110,20,100,110),
            array(150,50,100,150),
            array(120,0,100,100),
            array(null,0,100,100),
            array(null,120,100,220),
            array(120,null,100,120),
            array(null,null,100,PHP_INT_MAX),
        );
    }

    /**
     * @dataProvider effectiveMaxDataProvider
     */
    function testCalculatesEffectiveMax($max, $maxRise, $price, $result) 
    {
        $calculator = new Calculator();
        $calculator->setMax($max);
        $calculator->setMaxRise($maxRise);
        $this->assertEquals($result, $calculator->getEffectiveMax($price));
    }  

    function minMaxDataProvider() {
        return array(
            array(Calculator::TYPE_ADD, 100, 1, 101, 0, 1000),
            array(Calculator::TYPE_ADD, 100, 50, 125, 0, 125),
            array(Calculator::TYPE_ADD, 100, -50, 75, 75, 100),
        );
    }

    /**
     * @dataProvider minMaxDataProvider
     */
    function testAppliesMinMax($type, $value, $adjustment, $result, $min, $max) 
    {
        $calculator = new Calculator();
        $calculator->setMin($min);
        $calculator->setMax($max);
        $calculator->setAdjustmentType($type);
        $calculator->setAdjustmentValue($adjustment);
        $this->assertEquals($result, $calculator->adjust($value));
    }

    public function calculatorSetAndGetTest()
    {
        return array(
            array('Min',1),
            array('Max',2),
            array('MaxDrop',3),
            array('MaxRise',4),
            array('DecimalPlaces',2),
            array('AdjustmentType','TYPE'),
            array('AdjustmentValue',1),
            array('EnforcePriceWithinMinMax',true)
        );
    }
    
    /**
     * @dataProvider calculatorSetAndGetTest
     */
    public function testSetAndGet($name, $value)
    {
        $entity = new Calculator();

        $method = 'set' . $name;
        $entity->$method($value);

        $method = 'get' . $name;
        $this->assertEquals($value, $entity->$method(), 'Wrong value with ' . $name);
    }        
}